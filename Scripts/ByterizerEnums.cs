﻿using UnityEngine;

namespace GameSavvy.Byterizer
{
	/// <summary>
	/// The type to encode the length of an array.  
	/// Important for the encoding size
	/// </summary>
	public enum ArrayLengthType : byte
	{
		BYTE,
		UINT16,
		UNIT32
	}
}
