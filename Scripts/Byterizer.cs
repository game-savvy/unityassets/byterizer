using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using UnityEngine;

namespace GameSavvy.Byterizer
{
    /// <summary>
    /// Class that allows the serialization and de-serialization of parameters into a Byte Array
    /// </summary>
    public class ByteStream
    {
        private int _Index;

        /// <summary>
        /// The Index of the current byte to be popped
        /// </summary>
        /// <value>Between 0 and the length of the buffer</value>
        public int Index
        {
            get => _Index;

            set
            {
                if (value < 0)
                {
                    throw new Exception($"Invalid index, must be positive: [{value}]");
                }
                else if (value >= _Buff.Length)
                {
                    throw new Exception($"Invalid index, must be < than the length of the Buffer:  [{value}]");
                }
                else
                {
                    _Index = value;
                }
            }
        }

        /// <summary>
        /// The length of the byte[] Buffer
        /// </summary>
        public int Length => _Buff.Length;

        private byte[] _Buff;

        #region Constructors

        /// <summary>
        /// Creates an empty ByteStream class
        /// </summary>
        public ByteStream() => ResetBuffer();

        /// <summary>
        /// Initializes the ByteStream to contain a predefined bye[] as data
        /// </summary>
        /// <param name="sourceArray">The source array to copy from</param>
        /// <param name="copyLength">How many bytes to copy</param>
        /// <returns></returns>
        public ByteStream(byte[] sourceArray, int copyLength = -1) => Load(sourceArray, copyLength);

        /// <summary>
        /// Initializes the ByteStream to contain a predefined bye[] from another ByteStream
        /// </summary>
        /// <param name="source">The source ByteStream instance</param>
        /// <param name="startFrom">The index to start copying from</param>
        public ByteStream(ByteStream source, int startFrom = 0)
        {
            ResetBuffer();
            if (source.Length <= 0) return;
            _Buff = new byte[source.Length - startFrom];
            Buffer.BlockCopy(source.ToArray(), startFrom, _Buff, 0, source.Length - startFrom);
        }

        #endregion Constructors


        #region ByterizerFunctions

        /// <summary>
        /// Resets the container to be an Array.Empty<byte>() and resets the Index to 0
        /// </summary>
        public void ResetBuffer()
        {
            _Buff  = Array.Empty<byte>();
            _Index = 0;
        }

        /// <returns>Returns the Buffer byte[]</returns>
        public byte[] ToArray() => _Buff;

        /// <summary>
        /// Loads the ByteStream to contain a predefined bye[] from another ByteStream
        /// </summary>
        /// <param name="source">The source ByteStream instance</param>
        /// <param name="length">How many bytes to copy</param>
        public void Load(ByteStream source, int length = -1) => Load(source.ToArray(), length);

        /// <summary>
        /// Loads the ByteStream to contain a predefined bye[]
        /// </summary>
        /// <param name="source">The source ByteStream instance</param>
        /// <param name="length">How many bytes to copy</param>
        public void Load(byte[] source, int length = -1)
        {
            if (length == -1)
            {
                length = source.Length;
            }

            ResetBuffer();
            if (length == 0) return;
            _Buff = new byte[length];
            Buffer.BlockCopy(source, 0, _Buff, 0, length);
        }

        #endregion ByterizerFunctions


        #region Encoding

        /// <summary>
        ///     Easily encode multiple parameters (up to 255) into the ByteStream Buffer.  
        ///     Keep in mind the order of the parameters for when you need to Pop them out.  
        ///     When encoding an array alone, it will not be encoded as an array it will be encoded as a list of parameters
        ///     NOTE: Using this is slightly slower than pushing parameters one by one.  
        ///     NOTE: An array counts as 1 parameter.  
        /// </summary>
        /// <param name="args">the list of parameters to encode</param>
        /// <example>
        ///     Sample code showing how to encode several parameters including a string array.  
        ///     <code>
        ///     ByteStream bs = new ByteStream();
        ///     bs.Encode(1, 2f, 'A', new string[] { "Hello", "World" }, (byte)2, Vector3.Up);
        ///     </code>
        /// </example>
        public void Encode(params object[] args)
        {
            if (args.Length > 255)
            {
                throw new Exception("Encoding Error: Max Number of arguments is 255!");
            }

            ResetBuffer();
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] == null)
                {
                    throw new Exception(string.Format("Encoding Error: Invalid argument [{0}] :: value -> NULL", i));
                }

                EncodeObj(args[i]);
            }
        }

        private void EncodeObj(object obj)
        {
            switch (obj)
            {
                case null:
                    return;
                case bool val:
                    Append(val);
                    break;
                case byte val:
                    Append(val);
                    break;
                case sbyte val:
                    Append(val);
                    break;
                case ushort val:
                    Append(val);
                    break;
                case uint val:
                    Append(val);
                    break;
                case ulong val:
                    Append(val);
                    break;
                case short val:
                    Append(val);
                    break;
                case int val:
                    Append(val);
                    break;
                case long val:
                    Append(val);
                    break;
                case float val:
                    Append(val);
                    break;
                case double val:
                    Append(val);
                    break;
                case char val:
                    Append(val);
                    break;
                case string val:
                    Append(val);
                    break;
                case DateTime val:
                    Append(val);
                    break;
                case Vector2 v2:
                    Append(v2);
                    break;
                case Vector3 v2:
                    Append(v2);
                    break;
                case Quaternion val:
                    Append(val);
                    break;
                case bool[] arr:
                    AppendArray(arr);
                    break;
                case byte[] arr:
                    AppendArray(arr);
                    break;
                case sbyte[] arr:
                    AppendArray(arr);
                    break;
                case ushort[] arr:
                    AppendArray(arr);
                    break;
                case uint[] arr:
                    AppendArray(arr);
                    break;
                case ulong[] arr:
                    AppendArray(arr);
                    break;
                case short[] arr:
                    AppendArray(arr);
                    break;
                case int[] arr:
                    AppendArray(arr);
                    break;
                case long[] arr:
                    AppendArray(arr);
                    break;
                case float[] arr:
                    AppendArray(arr);
                    break;
                case double[] arr:
                    AppendArray(arr);
                    break;
                case char[] arr:
                    AppendArray(arr);
                    break;
                case string[] arr:
                    AppendArray(arr);
                    break;
                case DateTime[] arr:
                    AppendArray(arr);
                    break;
                case Vector2[] arr:
                    AppendArray(arr);
                    break;
                case Vector3[] arr:
                    AppendArray(arr);
                    break;
                case Quaternion[] arr:
                    AppendArray(arr);
                    break;
                default:
                    AppendSerializable(obj);
                    break;
            }
        }

        #endregion Encoding


        #region Append

        public void Append(bool val)
        {
            _Buff = ByteUtils.ConcatByteArrays(_Buff, new[] {(byte) (val ? 1 : 0)});
        }

        public void Append(byte val)
        {
            _Buff = ByteUtils.ConcatByteArrays(_Buff, new[] {val});
        }

        public void Append(sbyte val)
        {
            _Buff = ByteUtils.ConcatByteArrays(_Buff, new[] {(byte) val});
        }

        public void Append(short val)
        {
            _Buff = ByteUtils.ConcatByteArrays(_Buff, BitConverter.GetBytes(val));
        }

        public void Append(int val)
        {
            _Buff = ByteUtils.ConcatByteArrays(_Buff, BitConverter.GetBytes(val));
        }

        public void Append(long val)
        {
            _Buff = ByteUtils.ConcatByteArrays(_Buff, BitConverter.GetBytes(val));
        }

        public void Append(ushort val)
        {
            _Buff = ByteUtils.ConcatByteArrays(_Buff, BitConverter.GetBytes(val));
        }

        public void Append(uint val)
        {
            _Buff = ByteUtils.ConcatByteArrays(_Buff, BitConverter.GetBytes(val));
        }

        public void Append(ulong val)
        {
            _Buff = ByteUtils.ConcatByteArrays(_Buff, BitConverter.GetBytes(val));
        }

        public void Append(float val)
        {
            _Buff = ByteUtils.ConcatByteArrays(_Buff, BitConverter.GetBytes(val));
        }

        public void Append(double val)
        {
            _Buff = ByteUtils.ConcatByteArrays(_Buff, BitConverter.GetBytes(val));
        }

        public void Append(char val)
        {
            _Buff = ByteUtils.ConcatByteArrays(_Buff, BitConverter.GetBytes(val));
        }

        public void Append(DateTime val)
        {
            _Buff = ByteUtils.ConcatByteArrays(_Buff, BitConverter.GetBytes(val.Ticks));
        }

        public void Append(string val)
        {
            byte[] barr = Encoding.Unicode.GetBytes(val);
            _Buff = ByteUtils.ConcatByteArrays(_Buff, BitConverter.GetBytes(barr.Length), barr);
        }

        public void Append(Vector2 v2)
        {
            Append(v2.x);
            Append(v2.y);
        }

        public void Append(Vector3 v3)
        {
            Append(v3.x);
            Append(v3.y);
            Append(v3.z);
        }

        public void Append(Quaternion quat)
        {
            Append(quat.x);
            Append(quat.y);
            Append(quat.z);
            Append(quat.w);
        }

        /// <summary>
        /// Appends a serializable object to the end of the byte array, uses normal serialization.  
        /// Not recommended to use frequently, try to avoid a much as possible.  
        /// Only here in case that its really necessary.  
        /// </summary>
        /// <param name="obj">Must be a serilizable object</param>
        public void AppendSerializable(object obj)
        {
            if (obj == null)
            {
                return;
            }

            if (!obj.GetType().IsSerializable)
            {
                throw new Exception("Object [" + obj.GetType().Name + "] should be serializable!");
            }

            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream    ms = new MemoryStream();
            bf.Serialize(ms, obj);
            byte[] barr = ms.ToArray();
            _Buff = ByteUtils.ConcatByteArrays(_Buff, BitConverter.GetBytes(barr.Length), barr);
        } //AppendSerializable

        #endregion Append


        #region Prepend

        public void Prepend(bool val)
        {
            _Buff = ByteUtils.ConcatByteArrays(new[] {(byte) (val ? 1 : 0)}, _Buff);
        }

        public void Prepend(byte val)
        {
            _Buff = ByteUtils.ConcatByteArrays(new[] {val}, _Buff);
        }

        public void Prepend(sbyte val)
        {
            _Buff = ByteUtils.ConcatByteArrays(new[] {(byte) val}, _Buff);
        }

        public void Prepend(short val)
        {
            _Buff = ByteUtils.ConcatByteArrays(BitConverter.GetBytes(val), _Buff);
        }

        public void Prepend(int val)
        {
            _Buff = ByteUtils.ConcatByteArrays(BitConverter.GetBytes(val), _Buff);
        }

        public void Prepend(long val)
        {
            _Buff = ByteUtils.ConcatByteArrays(BitConverter.GetBytes(val), _Buff);
        }

        public void Prepend(ushort val)
        {
            _Buff = ByteUtils.ConcatByteArrays(BitConverter.GetBytes(val), _Buff);
        }

        public void Prepend(uint val)
        {
            _Buff = ByteUtils.ConcatByteArrays(BitConverter.GetBytes(val), _Buff);
        }

        public void Prepend(ulong val)
        {
            _Buff = ByteUtils.ConcatByteArrays(BitConverter.GetBytes(val), _Buff);
        }

        public void Prepend(float val)
        {
            _Buff = ByteUtils.ConcatByteArrays(BitConverter.GetBytes(val), _Buff);
        }

        public void Prepend(double val)
        {
            _Buff = ByteUtils.ConcatByteArrays(BitConverter.GetBytes(val), _Buff);
        }

        public void Prepend(char val)
        {
            _Buff = ByteUtils.ConcatByteArrays(BitConverter.GetBytes(val), _Buff);
        }

        public void Prepend(DateTime val)
        {
            _Buff = ByteUtils.ConcatByteArrays(BitConverter.GetBytes(val.Ticks), _Buff);
        }

        public void Prepend(string val)
        {
            byte[] barr = Encoding.Unicode.GetBytes(val);
            _Buff = ByteUtils.ConcatByteArrays(BitConverter.GetBytes(barr.Length), barr, _Buff);
        }

        public void Prepend(Vector2 v2)
        {
            Prepend(v2.y);
            Prepend(v2.x);
        }

        public void Prepend(Vector3 v3)
        {
            Prepend(v3.z);
            Prepend(v3.y);
            Prepend(v3.x);
        }

        public void Prepend(Quaternion quat)
        {
            Prepend(quat.w);
            Prepend(quat.z);
            Prepend(quat.y);
            Prepend(quat.x);
        }

        /// <summary>
        /// Prepends a serializable object to the beginning of the byte array, uses normal serialization.  
        /// Not recommended to use frequently, try to avoid a much as possible.  
        /// Only here in case that its really necessary.  
        /// </summary>
        /// <param name="obj">Must be a serializable object</param>
        public void PrependSerializable(object obj)
        {
            if (obj == null)
            {
                return;
            }

            if (!obj.GetType().IsSerializable)
            {
                throw new Exception("Object [" + obj.GetType().Name + "] should be serializable!");
            }

            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream    ms = new MemoryStream();
            bf.Serialize(ms, obj);
            byte[] barr = ms.ToArray();
            _Buff = ByteUtils.ConcatByteArrays(BitConverter.GetBytes(barr.Length), barr, _Buff);
        } //PrependSerializable

        #endregion Prepend


        #region AppendArrays

        private void TryIncludeArrayLengthAsType(bool includeLength, int arrayLength, ArrayLengthType typeToConvertTo)
        {
            if (includeLength == false)
            {
                return;
            }

            switch (typeToConvertTo)
            {
                case ArrayLengthType.BYTE:
                {
                    Append((byte) arrayLength);
                    break;
                }
                case ArrayLengthType.UINT16:
                {
                    Append((ushort) arrayLength);
                    break;
                }
                case ArrayLengthType.UNIT32:
                {
                    Append((uint) arrayLength);
                    break;
                }
            }
        }

        public void AppendArray(bool[]          arr, bool includeLength = true,
                                ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            TryIncludeArrayLengthAsType(includeLength, arr.Length, arrayLengthType);

            byte ix = 0;
            byte bb = 0;
            for (int i = 0; i < arr.Length; ++i)
            {
                bb |= (byte) (arr[i] ? Math.Pow(2, ix) : 0);
                ++ix;
                if (ix > 7)
                {
                    Append(bb);
                    ix = 0;
                    bb = 0;
                }
            }

            Append(bb);
        }

        public void AppendArray(byte[]          arr, bool includeLength = true,
                                ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            TryIncludeArrayLengthAsType(includeLength, arr.Length, arrayLengthType);
            _Buff = ByteUtils.ConcatByteArrays(_Buff, arr);
        }

        public void AppendArray(sbyte[]         arr, bool includeLength = true,
                                ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            TryIncludeArrayLengthAsType(includeLength, arr.Length, arrayLengthType);
            for (int i = 0; i < arr.Length; i++)
            {
                Append(arr[i]);
            }
        }

        public void AppendArray(short[]         arr, bool includeLength = true,
                                ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            TryIncludeArrayLengthAsType(includeLength, arr.Length, arrayLengthType);
            for (int i = 0; i < arr.Length; ++i)
            {
                Append(arr[i]);
            }
        }

        public void AppendArray(int[]           arr, bool includeLength = true,
                                ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            TryIncludeArrayLengthAsType(includeLength, arr.Length, arrayLengthType);
            for (int i = 0; i < arr.Length; ++i)
            {
                Append(arr[i]);
            }
        }

        public void AppendArray(long[]          arr, bool includeLength = true,
                                ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            TryIncludeArrayLengthAsType(includeLength, arr.Length, arrayLengthType);
            for (int i = 0; i < arr.Length; ++i)
            {
                Append(arr[i]);
            }
        }

        public void AppendArray(ushort[]        arr, bool includeLength = true,
                                ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            TryIncludeArrayLengthAsType(includeLength, arr.Length, arrayLengthType);
            for (int i = 0; i < arr.Length; ++i)
            {
                Append(arr[i]);
            }
        }

        public void AppendArray(uint[]          arr, bool includeLength = true,
                                ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            TryIncludeArrayLengthAsType(includeLength, arr.Length, arrayLengthType);
            for (int i = 0; i < arr.Length; ++i)
            {
                Append(arr[i]);
            }
        }

        public void AppendArray(ulong[]         arr, bool includeLength = true,
                                ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            TryIncludeArrayLengthAsType(includeLength, arr.Length, arrayLengthType);
            for (int i = 0; i < arr.Length; ++i)
            {
                Append(arr[i]);
            }
        }

        public void AppendArray(float[]         arr, bool includeLength = true,
                                ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            TryIncludeArrayLengthAsType(includeLength, arr.Length, arrayLengthType);
            for (int i = 0; i < arr.Length; ++i)
            {
                Append(arr[i]);
            }
        }

        public void AppendArray(double[]        arr, bool includeLength = true,
                                ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            TryIncludeArrayLengthAsType(includeLength, arr.Length, arrayLengthType);
            for (int i = 0; i < arr.Length; ++i)
            {
                Append(arr[i]);
            }
        }

        public void AppendArray(char[]          arr, bool includeLength = true,
                                ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            TryIncludeArrayLengthAsType(includeLength, arr.Length, arrayLengthType);
            for (int i = 0; i < arr.Length; ++i)
            {
                Append(arr[i]);
            }
        }

        public void AppendArray(DateTime[]      arr, bool includeLength = true,
                                ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            TryIncludeArrayLengthAsType(includeLength, arr.Length, arrayLengthType);
            for (int i = 0; i < arr.Length; ++i)
            {
                Append(arr[i]);
            }
        }

        public void AppendArray(string[]        arr, bool includeLength = true,
                                ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            TryIncludeArrayLengthAsType(includeLength, arr.Length, arrayLengthType);
            for (int i = 0; i < arr.Length; ++i)
            {
                Append(arr[i]);
            }
        }

        public void AppendArray(Vector2[]       arr, bool includeLength = true,
                                ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            TryIncludeArrayLengthAsType(includeLength, arr.Length, arrayLengthType);
            for (int i = 0; i < arr.Length; ++i)
            {
                Append(arr[i]);
            }
        }

        public void AppendArray(Vector3[]       arr, bool includeLength = true,
                                ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            TryIncludeArrayLengthAsType(includeLength, arr.Length, arrayLengthType);
            for (int i = 0; i < arr.Length; ++i)
            {
                Append(arr[i]);
            }
        }

        public void AppendArray(Quaternion[]    arr, bool includeLength = true,
                                ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            TryIncludeArrayLengthAsType(includeLength, arr.Length, arrayLengthType);
            for (int i = 0; i < arr.Length; ++i)
            {
                Append(arr[i]);
            }
        }

        public void AppendArray(object[]        arr, bool includeLength = true,
                                ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            TryIncludeArrayLengthAsType(includeLength, arr.Length, arrayLengthType);
            for (int i = 0; i < arr.Length; ++i)
            {
                AppendSerializable(arr[i]);
            }
        }

        #endregion AppendArrays


        #region Peek

        public byte PeekCurrentByte()
        {
            return _Buff[_Index];
        }

        public byte PeekByteAt(int byteIndex)
        {
            return _Buff[byteIndex];
        }

        public byte PeekFirstByte()
        {
            return _Buff[0];
        }

        public byte PeekLastByte()
        {
            return _Buff[_Buff.Length - 1];
        }

        public byte[] PeekBytes(int amount)
        {
            byte[] bytes = new byte[amount];
            for (int i = 0; i < amount; i++)
            {
                bytes[i] = _Buff[_Index + i];
            }

            return bytes;
        }

        #endregion Peek


        #region Pops

        public bool PopBool()
        {
            bool val = _Buff[_Index] == 1;
            ++_Index;
            return val;
        }

        public byte PopByte()
        {
            byte val = _Buff[_Index];
            ++_Index;
            return val;
        }

        public sbyte PopSByte()
        {
            sbyte val = (sbyte) _Buff[_Index];
            ++_Index;
            return val;
        }

        public short PopInt16()
        {
            short val = BitConverter.ToInt16(_Buff, _Index);
            _Index += 2;
            return val;
        }

        public int PopInt32()
        {
            int val = BitConverter.ToInt32(_Buff, _Index);
            _Index += 4;
            return val;
        }

        public long PopInt64()
        {
            long val = BitConverter.ToInt64(_Buff, _Index);
            _Index += 8;
            return val;
        }

        public ushort PopUInt16()
        {
            ushort val = BitConverter.ToUInt16(_Buff, _Index);
            _Index += 2;
            return val;
        }

        public uint PopUInt32()
        {
            uint val = BitConverter.ToUInt32(_Buff, _Index);
            _Index += 4;
            return val;
        }

        public ulong PopUInt64()
        {
            ulong val = BitConverter.ToUInt64(_Buff, _Index);
            _Index += 8;
            return val;
        }

        public float PopFloat()
        {
            float val = BitConverter.ToSingle(_Buff, _Index);
            _Index += sizeof(float);
            return val;
        }

        public double PopDouble()
        {
            double val = BitConverter.ToDouble(_Buff, _Index);
            _Index += sizeof(double);
            return val;
        }

        public char PopChar()
        {
            char val = BitConverter.ToChar(_Buff, _Index);
            _Index += 2;
            return val;
        }

        public DateTime PopDatetime()
        {
            DateTime val = DateTime.FromBinary(BitConverter.ToInt64(_Buff, _Index));
            _Index += 8;
            return val;
        }

        public string PopString()
        {
            int    len = PopInt32();
            string val = Encoding.Unicode.GetString(_Buff, _Index, len);
            _Index += len;
            return val;
        }

        public Vector2 PopVector2()
        {
            float x = PopFloat();
            float y = PopFloat();
            return new Vector2(x, y);
        }

        public Vector3 PopVector3()
        {
            float x = PopFloat();
            float y = PopFloat();
            float z = PopFloat();
            return new Vector3(x, y, z);
        }

        public Quaternion PopQuaternion()
        {
            float x = PopFloat();
            float y = PopFloat();
            float z = PopFloat();
            float w = PopFloat();
            return new Quaternion(x, y, z, w);
        }

        public object PopSerializable()
        {
            int             len       = PopInt32();
            MemoryStream    memStream = new MemoryStream();
            BinaryFormatter binForm   = new BinaryFormatter();
            memStream.Write(_Buff, _Index, len);
            memStream.Seek(0, SeekOrigin.Begin);
            object obj;
            try
            {
                obj = binForm.Deserialize(memStream);
            }
            catch (Exception)
            {
                Console.WriteLine("ByteArrayToObject Error @ index " + _Index);
                return false;
            }

            _Index += len;
            return obj;
        }

        #endregion Pops


        #region PopArrays

        private int TryGetLengthFrom(int len, ArrayLengthType typeToConvertFrom)
        {
            if (len >= 0)
            {
                return len;
            }

            switch (typeToConvertFrom)
            {
                case ArrayLengthType.BYTE:
                {
                    return PopByte();
                }
                case ArrayLengthType.UINT16:
                {
                    return PopUInt16();
                }
                case ArrayLengthType.UNIT32:
                {
                    return (int) PopUInt32();
                }

                default:
                {
                    return 0;
                }
            }
        }

        public bool[] PopBoolArray(int len = -1, ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            int    count = TryGetLengthFrom(len, arrayLengthType);
            bool[] barr  = new bool[count];
            if (count == 0) return barr;

            byte tmp = PopByte();
            int  ix  = 0;
            for (int i = 0; i < count; ++i)
            {
                barr[i] = ((tmp & (byte) Math.Pow(2, ix)) == 0 ? false : true);
                ++ix;
                if (ix > 7)
                {
                    ix  = 0;
                    tmp = PopByte();
                }
            }

            return barr;
        }

        public byte[] PopByteArray(int len = -1, ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            int    count = TryGetLengthFrom(len, arrayLengthType);
            byte[] barr  = new byte[count];
            for (int i = 0; i < count; ++i)
            {
                barr[i] = PopByte();
            }

            return barr;
        }

        public sbyte[] PopSByteArray(int len = -1, ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            int     count = TryGetLengthFrom(len, arrayLengthType);
            sbyte[] barr  = new sbyte[count];
            for (int i = 0; i < count; ++i)
            {
                barr[i] = PopSByte();
            }

            return barr;
        }

        public short[] PopInt16Array(int len = -1, ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            int count = TryGetLengthFrom(len, arrayLengthType);
            var arr   = new short[count];
            for (int i = 0; i < count; ++i)
            {
                arr[i] = PopInt16();
            }

            return arr;
        }

        public int[] PopInt32Array(int len = -1, ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            int count = TryGetLengthFrom(len, arrayLengthType);
            var arr   = new int[count];
            for (int i = 0; i < count; ++i)
            {
                arr[i] = PopInt32();
            }

            return arr;
        }

        public long[] PopInt64Array(int len = -1, ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            int count = TryGetLengthFrom(len, arrayLengthType);
            var arr   = new long[count];
            for (int i = 0; i < count; ++i)
            {
                arr[i] = PopInt64();
            }

            return arr;
        }

        public ushort[] PopUInt16Array(int len = -1, ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            int count = TryGetLengthFrom(len, arrayLengthType);
            var arr   = new ushort[count];
            for (int i = 0; i < count; ++i)
            {
                arr[i] = PopUInt16();
            }

            return arr;
        }

        public uint[] PopUInt32Array(int len = -1, ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            int count = TryGetLengthFrom(len, arrayLengthType);
            var arr   = new uint[count];
            for (int i = 0; i < count; ++i)
            {
                arr[i] = PopUInt32();
            }

            return arr;
        }

        public ulong[] PopUInt64Array(int len = -1, ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            int count = TryGetLengthFrom(len, arrayLengthType);
            var arr   = new ulong[count];
            for (int i = 0; i < count; ++i)
            {
                arr[i] = PopUInt64();
            }

            return arr;
        }

        public float[] PopFloatArray(int len = -1, ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            int count = TryGetLengthFrom(len, arrayLengthType);
            var arr   = new float[count];
            for (int i = 0; i < count; ++i)
            {
                arr[i] = PopFloat();
            }

            return arr;
        }

        public double[] PopDoubleArray(int len = -1, ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            int count = TryGetLengthFrom(len, arrayLengthType);
            var arr   = new double[count];
            for (int i = 0; i < count; ++i)
            {
                arr[i] = PopDouble();
            }

            return arr;
        }

        public char[] PopCharArray(int len = -1, ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            int count = TryGetLengthFrom(len, arrayLengthType);
            var arr   = new char[count];
            for (int i = 0; i < count; ++i)
            {
                arr[i] = PopChar();
            }

            return arr;
        }

        public DateTime[] PopDateTimeArray(int len = -1, ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            int count = TryGetLengthFrom(len, arrayLengthType);
            var arr   = new DateTime[count];
            for (int i = 0; i < count; ++i)
            {
                arr[i] = PopDatetime();
            }

            return arr;
        }

        public string[] PopStringArray(int len = -1, ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            int count = TryGetLengthFrom(len, arrayLengthType);
            var arr   = new string[count];
            for (int i = 0; i < count; ++i)
            {
                arr[i] = PopString();
            }

            return arr;
        }

        public Vector2[] PopVector2Array(int len = -1, ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            int count = TryGetLengthFrom(len, arrayLengthType);
            var arr   = new Vector2[count];
            for (int i = 0; i < count; ++i)
            {
                arr[i] = PopVector2();
            }

            return arr;
        }

        public Vector3[] PopVector3Array(int len = -1, ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            int count = TryGetLengthFrom(len, arrayLengthType);
            var arr   = new Vector3[count];
            for (int i = 0; i < count; ++i)
            {
                arr[i] = PopVector3();
            }

            return arr;
        }

        public Quaternion[] PopQuaternionArray(int len = -1, ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            int count = TryGetLengthFrom(len, arrayLengthType);
            var arr   = new Quaternion[count];
            for (int i = 0; i < count; ++i)
            {
                arr[i] = PopQuaternion();
            }

            return arr;
        }

        public object[] PopSerializableArray(int len = -1, ArrayLengthType arrayLengthType = ArrayLengthType.BYTE)
        {
            int count = TryGetLengthFrom(len, arrayLengthType);
            var arr   = new object[count];
            for (int i = 0; i < count; ++i)
            {
                arr[i] = PopSerializable();
            }

            return arr;
        }

        public byte[] PopBytes(int amount)
        {
            byte[] bytes = new byte[amount];
            for (int i = 0; i < amount; i++)
            {
                bytes[i] = _Buff[_Index + i];
            }

            _Index += amount;
            return bytes;
        }

        #endregion PopArrays
    } //CLASS NetMessage
}