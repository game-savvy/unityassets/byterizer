using System;

namespace GameSavvy.Byterizer
{
	public static class BoolUtils
	{

		/// <summary>
		/// Wraps up to the first 8 bools from a bool[] into a single byte.  
		/// </summary>
		/// <param name="bools">the bool array</param>
		/// <returns></returns>
		public static byte WrapBoolsIntoByte(params bool[] bools)
		{
			byte ix = 0;
			byte bb = 0;
			for (int i = 0; i < bools.Length; ++i)
			{
				bb |= (byte)(bools[i] ? Math.Pow(2, ix) : 0);
				++ix;
				if (ix > 7) return bb;
			}
			return bb;
		}

		/// <summary>
		/// Wraps up to the first 16 bools from a bool[] into a UInt16.  
		/// </summary>
		/// <param name="bools">the bool array</param>
		/// <returns></returns>
		public static UInt16 WrapBoolsIntoShort(params bool[] bools)
		{
			int ix = 0;
			UInt16 bb = 0;
			for (int i = 0; i < bools.Length; ++i)
			{
				bb |= (UInt16)(bools[i] ? Math.Pow(2, ix) : 0);
				++ix;
				if (ix > 15) return bb;
			}
			return bb;
		}

		/// <summary>
		/// Wraps up to the first 32 bools from a bool[] into a UInt32.  
		/// </summary>
		/// <param name="bools">the bool array</param>
		/// <returns></returns>
		public static UInt32 WrapBoolsIntoInt(params bool[] bools)
		{
			int ix = 0;
			UInt32 bb = 0;
			for (int i = 0; i < bools.Length; ++i)
			{
				bb |= (UInt32)(bools[i] ? Math.Pow(2, ix) : 0);
				++ix;
				if (ix > 31) return bb;
			}
			return bb;
		}

		/// <summary>
		/// Wraps up to the first 64 bools from a bool[] into a UInt64.  
		/// </summary>
		/// <param name="bools">the bool array</param>
		/// <returns></returns>
		public static UInt64 WrapBoolsIntoLong(params bool[] bools)
		{
			int ix = 0;
			UInt64 bb = 0;
			for (int i = 0; i < bools.Length; ++i)
			{
				bb |= (UInt64)(bools[i] ? Math.Pow(2, ix) : 0);
				++ix;
				if (ix > 63) return bb;
			}
			return bb;
		}

		/// <returns>Returns a bool based on the value of the bit at "index" from sourceByte</returns>
		public static bool GetBoolAt(byte sourceByte, int index)
		{
			if (index > 7) return false;
			return ((sourceByte & (byte)Math.Pow(2, index)) > 0 ? true : false);
		}

		/// <returns>Returns a bool based on the value of the bit at "index" from sourceUInt16</returns>
		public static bool GetBoolAt(UInt16 sourceUInt16, int index)
		{
			if (index > 15) return false;
			return ((sourceUInt16 & (UInt16)Math.Pow(2, index)) > 0 ? true : false);
		}

		/// <returns>Returns a bool based on the value of the bit at "index" from sourceUInt32</returns>
		public static bool GetBoolAt(UInt32 sourceUInt32, int index)
		{
			if (index > 31) return false;
			return ((sourceUInt32 & (UInt32)Math.Pow(2, index)) > 0 ? true : false);
		}

		/// <returns>Returns a bool based on the value of the bit at "index" from sourceUInt64</returns>
		public static bool GetBoolAt(UInt64 sourceUInt64, int index)
		{
			if (index > 63) return false;
			return ((sourceUInt64 & (UInt64)Math.Pow(2, index)) > 0 ? true : false);
		}
	}
}
