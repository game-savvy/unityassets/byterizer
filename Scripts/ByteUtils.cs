using System;
using System.Linq;

namespace GameSavvy.Byterizer
{
	public static class ByteUtils
	{

		/// <summary>
		/// Extremely fast way of creating a new byte[] by concatenating 2 arrays
		/// </summary>
		/// <param name="first">The first array to concatenate</param>
		/// <param name="second">The second array to concatenate</param>
		public static byte[] ConcatByteArrays(byte[] first, byte[] second)
		{
			byte[] ret = new byte[first.Length + second.Length];
			Buffer.BlockCopy(first, 0, ret, 0, first.Length);
			Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
			return ret;
		}

		/// <summary>
		/// Extremely fast way of creating a new byte[] by concatenating 3 arrays
		/// </summary>
		/// <param name="first">The first array to concatenate</param>
		/// <param name="second">The second array to concatenate</param>
		/// <param name="third">The third array to concatenate</param>
		public static byte[] ConcatByteArrays(byte[] first, byte[] second, byte[] third)
		{
			byte[] ret = new byte[first.Length + second.Length + third.Length];
			Buffer.BlockCopy(first, 0, ret, 0, first.Length);
			Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
			Buffer.BlockCopy(third, 0, ret, first.Length + second.Length, third.Length);
			return ret;
		}

		/// <summary>
		/// Extremely fast way of creating a new byte[] by concatenating a N number of arrays
		/// </summary>
		/// <param name="arrays">an array of arrays to concatenate together</param>
		public static byte[] ConcatByteArrays(params byte[][] arrays)
		{
			byte[] ret = new byte[arrays.Sum(x => x.Length)];
			int offset = 0;
			for (int i = 0; i < arrays.Length; i++)
			{
				Buffer.BlockCopy(arrays[i], 0, ret, offset, arrays[i].Length);
				offset += arrays[i].Length;
			}
			return ret;
		}

		/// <summary>
		/// Extremely fast way of getting a sub byte[] from another byte[]
		/// </summary>
		/// <param name="sourceArray">The source array to get the sub array from</param>
		/// <param name="index">the index to start the sub array from the sourceArray</param>
		/// <param name="length">how many bytes to get from the sourceArray "-1 = until the end"</param>
		public static byte[] SubByteArray(byte[] sourceArray, int index, int length = -1)
		{
			if (index > sourceArray.Length || index < 0)
			{
				return null;
			}

			if(length < 0)
			{
				length = sourceArray.Length - index;
			}
			
			var result = new byte[length];
			Buffer.BlockCopy(sourceArray, index, result, 0, length);
			return result;
		}

	}
}
