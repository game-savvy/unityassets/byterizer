using System;

namespace GameSavvy.Byterizer
{
	public static class BitUtils
	{

		/// <summary>
		/// Pushes an UnSigned number clamped to a specific amount of bits into a UInt64.  
		/// Keep in mind that the "ref whereToPush" will offset its current bits to make space for the new bits
		/// </summary>
		/// <param name="whatToPush">the unsigned number to push</param>
		/// <param name="howManyBits">how many bits to push</param>
		/// <param name="whereToPush">the reference to where to push the bits</param>
		/// <example>
		/// push the number 1 with 4 bits, then push the number 3 with 4 bits
		/// <code>
		/// UInt64 result = 0;
		/// var tmp BitUtils.PushBits((UInt64)1, 4, ref result);
		/// //result should now have [0001]
		/// var tmp BitUtils.PushBits((UInt64)3, 4, ref result);
		/// //result should now have [00010011]
		/// </code>
		/// </example>
		public static void PushBits(UInt64 whatToPush, byte howManyBits, ref UInt64 whereToPush)
		{
			UInt64 mask = (UInt64)(((UInt64)1 << howManyBits) - 1);
			UInt64 num = (UInt64)(whatToPush & mask);
			whereToPush = (UInt64)((whereToPush << howManyBits) | num);
		}

		/// <summary>
		/// Pushes a Signed number clamped to a specific amount of bits into a UInt64.  
		/// The last bit is always used as the sign bit
		/// Keep in mind that the "ref whereToPush" will offset its current bits to make space for the new bits.  
		/// </summary>
		/// <param name="whatToPush">the Signed number to push</param>
		/// <param name="howManyBits">how many bits to push</param>
		/// <param name="whereToPush">the reference to where to push the bits</param>
		/// <example>
		/// push the number -1 with 4 bits, then push the number -3 with 6 bits, then push the number +1 wiht 4 bits
		/// <code>
		/// UInt64 result = 0;
		/// var tmp BitUtils.PushBits((UInt64)-1, 4, ref result);
		/// //result should now have [1001]
		/// var tmp BitUtils.PushBits((UInt64)-3, 6, ref result);
		/// //result should now have [1001 100011]
		/// var tmp BitUtils.PushBits((UInt64)1, 4, ref result);
		/// //result should now have [1001 100011 0001]
		/// </code>
		/// </example>
		public static void PushBitsSigned(Int64 whatToPush, byte howManyBits, ref UInt64 whereToPush)
		{
			UInt64 mask = (UInt64)(((UInt64)1 << howManyBits - 1) - 1);
			UInt64 sign = 0;
			if (whatToPush < 0)
			{
				sign = (UInt64)(1 << (howManyBits - 1));
				whatToPush *= -1;
			}

			UInt64 num = (UInt64)(((UInt64)whatToPush & mask) | sign);
			whereToPush = (whereToPush << howManyBits) | num;
		}

		/// <summary>
		/// Pops a specific amount of bits from a UInt64 as a UInt64.  
		/// The "whereToPopFrom" number will have its bits shifted based on the bits to pop amount.  
		/// </summary>
		/// <param name="whereToPopFrom">The UInt64 to pop from</param>
		/// <param name="howManyBits">the amount of bits to pop</param>
		/// <example>
		/// pops two 4bit numbers
		/// <code>
		/// UInt64 popFrom = 0x13;
		/// var num = BitUtils.PopBits(ref popFrom, 4);
		/// // num = 3, popFrom = 0x1
		/// num = BitUtils.PopBits(ref popFrom, 4);
		/// // num = 1, popFrom = 0x0
		/// </code>
		/// </example>
		public static UInt64 PopBits(ref UInt64 whereToPopFrom, byte howManyBits)
		{
			UInt64 mask = (UInt64)(((UInt64)1 << howManyBits) - 1);
			UInt64 result = (whereToPopFrom & mask);
			whereToPopFrom = (ulong)(whereToPopFrom >> howManyBits);
			return result;
		}

		/// <summary>
		/// Pops a specific amount of bits from a UInt64 as a Signed Int64.  
		/// The "whereToPopFrom" number will have its bits shifted based on the bits to pop amount.  
		/// The last bit is always counted as the sign (1 = negative, 0 = positive)
		/// </summary>
		/// <param name="whereToPopFrom">The UInt64 to pop from</param>
		/// <param name="howManyBits">the amount of bits to pop</param>
		/// <example>
		/// pops two signed 4bit numbers
		/// <code>
		/// UInt64 popFrom = 0x9B;//10011011
		/// var num = BitUtils.PopBits(ref popFrom, 4);
		/// // num = -3, popFrom = 0x9
		/// num = BitUtils.PopBits(ref popFrom, 4);
		/// // num = -1, popFrom = 0x0
		/// </code>
		/// </example>
		public static Int64 PopBitsSigned(ref UInt64 whereToPopFrom, byte howManyBits)
		{
			UInt64 mask = (UInt64)(1 << (howManyBits - 1)) - 1;
			UInt64 sign = (UInt64)(1 << (howManyBits - 1));
			Int64 result = (Int64)(whereToPopFrom & mask);
			if ((whereToPopFrom & sign) != 0)
			{
				result *= -1;
			}
			whereToPopFrom = (UInt64)(whereToPopFrom >> howManyBits);
			return result;
		}

		/// <summary>
		/// Pops a specific amount of bits into a byte.  
		/// "whereToPopFrom" has its bits shifted based on the bitCount.  
		/// </summary>
		/// <param name="whereToPopFrom">ref variable to pop bits from</param>
		/// <param name="bitCount">how many bits to pop (Max 8)</param>
		/// <returns>returns an unsigned byte</returns>
		public static byte PopByte(ref UInt64 whereToPopFrom, byte bitCount = 8)
		{
			if (bitCount > 8)
			{
				throw new Exception(string.Format("Error: BitCount too big [{0}]... must be <= 8", bitCount));
			}

			UInt64 rawData =  PopBits(ref whereToPopFrom, bitCount);
			return (byte)rawData;
		}

		/// <summary>
		/// Pops a specific amount of bits into a sbyte.  
		/// "whereToPopFrom" has its bits shifted based on the bitCount.  
		/// the last bit is always counted as the sign (1 = negative, 0 = positive)
		/// </summary>
		/// <param name="whereToPopFrom">ref variable to pop bits from</param>
		/// <param name="bitCount">how many bits to pop (Max 8)</param>
		/// <returns>returns an Signed byte</returns>
		public static sbyte PopSByte(ref UInt64 whereToPopFrom, byte bitCount = 8)
		{
			if (bitCount > 8)
			{
				throw new Exception(string.Format("Error: BitCount too big [{0}]... must be <= 8", bitCount));
			}

			Int64 rawData =  PopBitsSigned(ref whereToPopFrom, bitCount);
			return (sbyte)rawData;
		}

		/// <summary>
		/// Pops a specific amount of bits into a Int16.  
		/// "whereToPopFrom" has its bits shifted based on the bitCount.  
		/// the last bit is always counted as the sign (1 = negative, 0 = positive)
		/// </summary>
		/// <param name="whereToPopFrom">ref variable to pop bits from</param>
		/// <param name="bitCount">how many bits to pop (Max 16)</param>
		/// <returns>returns an Signed Int16</returns>
		public static Int16 PopInt16(ref UInt64 whereToPopFrom, byte bitCount = 16)
		{
			if (bitCount > 16)
			{
				throw new Exception(string.Format("Error: BitCount too big [{0}]... must be <= 16", bitCount));
			}

			Int64 rawData =  PopBitsSigned(ref whereToPopFrom, bitCount);
			return (Int16)rawData;
		}

		/// <summary>
		/// Pops a specific amount of bits into a UInt16.  
		/// "whereToPopFrom" has its bits shifted based on the bitCount.  
		/// </summary>
		/// <param name="whereToPopFrom">ref variable to pop bits from</param>
		/// <param name="bitCount">how many bits to pop (Max 16)</param>
		/// <returns>returns an UInt16</returns>
		public static UInt16 PopUInt16(ref UInt64 whereToPopFrom, byte bitCount = 16)
		{
			if (bitCount > 16)
			{
				throw new Exception(string.Format("Error: BitCount too big [{0}]... must be <= 16", bitCount));
			}

			UInt64 rawData =  PopBits(ref whereToPopFrom, bitCount);
			return (UInt16)rawData;
		}

		/// <summary>
		/// Pops a specific amount of bits into a Int32.  
		/// "whereToPopFrom" has its bits shifted based on the bitCount.  
		/// the last bit is always counted as the sign (1 = negative, 0 = positive)
		/// </summary>
		/// <param name="whereToPopFrom">ref variable to pop bits from</param>
		/// <param name="bitCount">how many bits to pop (Max 32)</param>
		/// <returns>returns an Signed Int32</returns>
		public static Int32 PopInt32(ref UInt64 whereToPopFrom, byte bitCount = 32)
		{
			if (bitCount > 32)
			{
				throw new Exception(string.Format("Error: BitCount too big [{0}]... must be <= 32", bitCount));
			}

			Int64 rawData =  PopBitsSigned(ref whereToPopFrom, bitCount);
			return (Int32)rawData;
		}

		/// <summary>
		/// Pops a specific amount of bits into a UInt32.  
		/// "whereToPopFrom" has its bits shifted based on the bitCount.  
		/// </summary>
		/// <param name="whereToPopFrom">ref variable to pop bits from</param>
		/// <param name="bitCount">how many bits to pop (Max 32)</param>
		public static UInt32 PopUInt32(ref UInt64 whereToPopFrom, byte bitCount = 32)
		{
			if (bitCount > 32)
			{
				throw new Exception(string.Format("Error: BitCount too big [{0}]... must be <= 32", bitCount));
			}

			UInt64 rawData =  PopBits(ref whereToPopFrom, bitCount);
			return (UInt32)rawData;
		}

		/// <summary>
		/// Pops a specific amount of bits into a Int64.  
		/// "whereToPopFrom" has its bits shifted based on the bitCount.  
		/// the last bit is always counted as the sign (1 = negative, 0 = positive)
		/// </summary>
		/// <param name="whereToPopFrom">ref variable to pop bits from</param>
		/// <param name="bitCount">how many bits to pop (Max 63)</param>
		/// <returns>returns an Signed Int64</returns>
		public static Int64 PopInt64(ref UInt64 whereToPopFrom, byte bitCount = 63)
		{
			if (bitCount > 63)
			{
				throw new Exception(string.Format("Error: BitCount too big [{0}]... must be <= 63", bitCount));
			}

			Int64 rawData =  PopBitsSigned(ref whereToPopFrom, bitCount);
			return (Int64)rawData;
		}

		/// <summary>
		/// Pops a specific amount of bits into a UInt64.  
		/// "whereToPopFrom" has its bits shifted based on the bitCount.  
		/// </summary>
		/// <param name="whereToPopFrom">ref variable to pop bits from</param>
		/// <param name="bitCount">how many bits to pop (Max 63)</param>
		public static UInt64 PopUInt64(ref UInt64 whereToPopFrom, byte bitCount = 63)
		{
			if (bitCount > 63)
			{
				throw new Exception(string.Format("Error: BitCount too big [{0}]... must be <= 63", bitCount));
			}

			UInt64 rawData =  PopBits(ref whereToPopFrom, bitCount);
			return (UInt64)rawData;
		}

		/// <summary>
		/// Pops a the first available bit as a bool.  
		/// "whereToPopFrom" has its bits shifted by 1.  
		/// </summary>
		/// <param name="whereToPopFrom">ref variable to pop the bit from</param>
		public static bool PopBool(ref UInt64 whereToPopFrom)
		{
			UInt64 rawData =  PopBits(ref whereToPopFrom, 1);
			return ((byte)rawData) == 1;
		}

	}
}
