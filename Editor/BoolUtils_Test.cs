﻿#if UNITY_EDITOR

using NUnit.Framework;
using GameSavvy.Byterizer;


// ReSharper disable once CheckNamespace
namespace UnitTests.Byterizer.Tests
{
    public class BoolUtils_Test
    {
        [TestCase(0)]
        [TestCase(1, true)]
        [TestCase(5, true, false, true)]
        [TestCase(9, true, false, false, true)]
        [TestCase(17, true, false, false, false, true)]
        [TestCase(33, true, false, false, false, false, true)]
        [TestCase(65, true, false, false, false, false, false, true)]
        [TestCase(129, true, false, false, false, false, false, false, true)]
        [TestCase(129, new[] { true, false, false, false, false, false, false, true })]
        public void WrapBoolsIntoByte_Test(int num, params bool[] args)
        {
            var boolsByte = BoolUtils.WrapBoolsIntoByte(args);
            Assert.AreEqual(boolsByte, num);
        }

        [TestCase(0)]
        [TestCase(1, true)]
        [TestCase(5, true, false, true)]
        [TestCase(9, true, false, false, true)]
        [TestCase(17, true, false, false, false, true)]
        [TestCase(33, true, false, false, false, false, true)]
        [TestCase(65, true, false, false, false, false, false, true)]
        [TestCase(129, true, false, false, false, false, false, false, true)]
        [TestCase(257, true, false, false, false, false, false, false, false, true)]
        [TestCase(513, true, false, false, false, false, false, false, false, false, true)]
        [TestCase(1025, true, false, false, false, false, false, false, false, false, false, true)]
        [TestCase(2049, true, false, false, false, false, false, false, false, false, false, false, true)]
        [TestCase(2049, new[] { true, false, false, false, false, false, false, false, false, false, false, true })]
        public void WrapBoolsIntoShort_Test(int num, params bool[] args)
        {
            var boolsByte = BoolUtils.WrapBoolsIntoShort(args);
            Assert.AreEqual(boolsByte, num);
        }

        [TestCase(0)]
        [TestCase(1, true)]
        [TestCase(5, true, false, true)]
        [TestCase(9, true, false, false, true)]
        [TestCase(17, true, false, false, false, true)]
        [TestCase(33, true, false, false, false, false, true)]
        [TestCase(65, true, false, false, false, false, false, true)]
        [TestCase(129, true, false, false, false, false, false, false, true)]
        [TestCase(257, true, false, false, false, false, false, false, false, true)]
        [TestCase(513, true, false, false, false, false, false, false, false, false, true)]
        [TestCase(1025, true, false, false, false, false, false, false, false, false, false, true)]
        [TestCase(2049, true, false, false, false, false, false, false, false, false, false, false, true)]
        [TestCase(2049, new[] { true, false, false, false, false, false, false, false, false, false, false, true })]
        public void WrapBoolsIntoInt_Test(int num, params bool[] args)
        {
            var boolsByte = BoolUtils.WrapBoolsIntoInt(args);
            Assert.AreEqual(boolsByte, num);
        }


        [Test]
        public void WrapBoolsIntoInt_Test2()
        {
            var arr = new bool[32];
            arr[0] = true;
            arr[31] = true;
            var val = BoolUtils.WrapBoolsIntoLong(arr);
            const uint lng = 0x80000001;

            Assert.AreEqual(val, lng);
        }

        [TestCase(0)]
        [TestCase(1, true)]
        [TestCase(5, true, false, true)]
        [TestCase(9, true, false, false, true)]
        [TestCase(17, true, false, false, false, true)]
        [TestCase(33, true, false, false, false, false, true)]
        [TestCase(65, true, false, false, false, false, false, true)]
        [TestCase(129, true, false, false, false, false, false, false, true)]
        [TestCase(257, true, false, false, false, false, false, false, false, true)]
        [TestCase(513, true, false, false, false, false, false, false, false, false, true)]
        [TestCase(1025, true, false, false, false, false, false, false, false, false, false, true)]
        [TestCase(2049, true, false, false, false, false, false, false, false, false, false, false, true)]
        [TestCase(2049, new[] { true, false, false, false, false, false, false, false, false, false, false, true })]
        public void WrapBoolsIntoLong_Test(int num, params bool[] args)
        {
            var boolsByte = BoolUtils.WrapBoolsIntoLong(args);
            Assert.AreEqual(boolsByte, num);
        }

        [Test]
        public void WrapBoolsIntoLong_Test2()
        {
            var arr = new bool[64];
            arr[0] = true;
            arr[63] = true;
            var val = BoolUtils.WrapBoolsIntoLong(arr);
            const ulong lng = 0x8000000000000001;

            Assert.AreEqual(val, lng);
        }

        [TestCase((byte)0xFF, 0, true)]
        [TestCase((byte)0xFF, 3, true)]
        [TestCase((byte)0xFF, 7, true)]
        [TestCase((byte)0x8, 0, false)]
        [TestCase((byte)0x8, 7, false)]
        [TestCase((byte)0x8, 3, true)]
        [TestCase((byte)0x8, 4, false)]
        public void GetBoolAt_Byte_Test(byte val, int pos, bool desiredVal)
        {
            bool b = BoolUtils.GetBoolAt(val, pos);
            Assert.AreEqual(b, desiredVal);
        }

        [TestCase((ushort)0xFFFF, 0, true)]
        [TestCase((ushort)0xFFFF, 3, true)]
        [TestCase((ushort)0xFFFF, 7, true)]
        [TestCase((ushort)0xFFFF, 11, true)]
        [TestCase((ushort)0xFFFF, 15, true)]
        [TestCase((ushort)0x8001, 0, true)]
        [TestCase((ushort)0x8001, 3, false)]
        [TestCase((ushort)0x8001, 7, false)]
        [TestCase((ushort)0x8001, 11, false)]
        [TestCase((ushort)0x8001, 15, true)]
        public void GetBoolAt_UInt16_Test(ushort val, int pos, bool desiredVal)
        {
            bool b = BoolUtils.GetBoolAt(val, pos);
            Assert.AreEqual(desiredVal, b);
        }

        [TestCase(0xFFFFFFFF, 0, true)]
        [TestCase(0xFFFFFFFF, 7, true)]
        [TestCase(0xFFFFFFFF, 15, true)]
        [TestCase(0xFFFFFFFF, 23, true)]
        [TestCase(0xFFFFFFFF, 31, true)]
        [TestCase(0x80000001, 0, true)]
        [TestCase(0x80000001, 7, false)]
        [TestCase(0x80000001, 15, false)]
        [TestCase(0x80000001, 23, false)]
        [TestCase(0x80000001, 31, true)]
        public void GetBoolAt_UInt32_Test(uint val, int pos, bool desiredVal)
        {
            bool b = BoolUtils.GetBoolAt(val, pos);
            Assert.AreEqual(desiredVal, b);
        }

        [TestCase(0xFFFFFFFFFFFFFFFF, 0, true)]
        [TestCase(0xFFFFFFFFFFFFFFFF, 15, true)]
        [TestCase(0xFFFFFFFFFFFFFFFF, 31, true)]
        [TestCase(0xFFFFFFFFFFFFFFFF, 47, true)]
        [TestCase(0xFFFFFFFFFFFFFFFF, 63, true)]
        [TestCase(0x8000000000000001, 0, true)]
        [TestCase(0x8000000000000001, 15, false)]
        [TestCase(0x8000000000000001, 31, false)]
        [TestCase(0x8000000000000001, 47, false)]
        [TestCase(0x8000000000000001, 63, true)]
        public void GetBoolAt_UInt64_Test(ulong val, int pos, bool desiredVal)
        {
            bool b = BoolUtils.GetBoolAt(val, pos);
            Assert.AreEqual(desiredVal, b);
        }
    }
}


#endif