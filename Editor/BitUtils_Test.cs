﻿#if UNITY_EDITOR

using NUnit.Framework;
using GameSavvy.Byterizer;


// ReSharper disable once CheckNamespace
namespace UnitTests.Byterizer.Tests
{
    public class BitUtils_Test
    {
        [TestCase((ulong)0x0F, (byte)7, (ulong)0x0F)]
        [TestCase((ulong)0x7F, (byte)7, (ulong)0x7F)]
        [TestCase((ulong)0x70F, (byte)7, (ulong)0x0F)]
        [TestCase((ulong)0x70F, (byte)9, (ulong)0x10F)]
        [TestCase((ulong)0x70F, (byte)10, (ulong)0x30F)]
        public void PushPopBits_UInt64_Test(ulong bitsToPush, byte howMany, ulong desiredVal)
        {
            ulong result = 0;
            BitUtils.PushBits(bitsToPush, howMany, ref result);
            Assert.AreEqual(desiredVal, result);

            ulong got = BitUtils.PopBits(ref result, howMany);
            Assert.AreEqual(desiredVal, got);
        }

        [TestCase((ulong)0xF, (byte)16, (ulong)0xF000F)]
        [TestCase((ulong)0xF, (byte)12, (ulong)0xF00F)]
        [TestCase((ulong)0xF, (byte)8, (ulong)0xF0F)]
        [TestCase((ulong)0xF, (byte)7, (ulong)0x78F)]
        [TestCase((ulong)0xF, (byte)5, (ulong)0x1EF)]
        [TestCase((ulong)0xF, (byte)3, (ulong)0x3F)]
        [TestCase((ulong)0x81, (byte)32, (ulong)554050781313)]
        [TestCase((ulong)0x81, (byte)15, (ulong)4227201)]
        [TestCase((ulong)0x81, (byte)7, (ulong)129)]
        [TestCase((ulong)0x81, (byte)5, (ulong)33)]
        [TestCase((ulong)0x81, (byte)3, (ulong)0x9)]
        public void PushPopBits_UInt64_Test2(ulong bitsToPush, byte howMany, ulong desiredVal)
        {
            ulong result = 0;
            BitUtils.PushBits(bitsToPush, howMany, ref result);
            BitUtils.PushBits(bitsToPush, howMany, ref result);
            Assert.AreEqual(desiredVal, result);


            ulong reference = 0;
            BitUtils.PushBits(bitsToPush, howMany, ref reference);

            ulong got = BitUtils.PopBits(ref result, howMany);
            Assert.AreEqual(reference, got);

            got = BitUtils.PopBits(ref result, howMany);
            Assert.AreEqual(reference, got);
        }

        [TestCase((ulong)0x81, (byte)3, (ulong)73)]
        [TestCase((ulong)0x81, (byte)6, (ulong)4161)]
        [TestCase((ulong)0x81, (byte)9, (ulong)33882753)]
        public void PushBits_UInt64_Test3(ulong bitsToPush, byte howMany, ulong desiredVal)
        {
            ulong result = 0;
            BitUtils.PushBits(bitsToPush, howMany, ref result);
            BitUtils.PushBits(bitsToPush, howMany, ref result);
            BitUtils.PushBits(bitsToPush, howMany, ref result);
            Assert.AreEqual(desiredVal, result);


            ulong reference = 0;
            BitUtils.PushBits(bitsToPush, howMany, ref reference);

            ulong got = BitUtils.PopBits(ref result, howMany);
            Assert.AreEqual(reference, got);

            got = BitUtils.PopBits(ref result, howMany);
            Assert.AreEqual(reference, got);

            got = BitUtils.PopBits(ref result, howMany);
            Assert.AreEqual(reference, got);
        }

        [TestCase((long)(-1), (byte)3, (ulong)5)]
        [TestCase((long)(-1), (byte)4, (ulong)9)]
        [TestCase((long)(-1), (byte)5, (ulong)17)]
        [TestCase((long)(-7), (byte)4, (ulong)15)]
        [TestCase((long)(-7), (byte)5, (ulong)0x17)]
        public void PushBits_Int64_Test(long bitsToPush, byte howMany, ulong desiredVal)
        {
            ulong result = 0;
            BitUtils.PushBitsSigned(bitsToPush, howMany, ref result);
            Assert.AreEqual(desiredVal, result);

            long got = BitUtils.PopBitsSigned(ref result, howMany);
            Assert.AreEqual(bitsToPush, got);
        }

        [TestCase((long)(-1), (byte)3, (ulong)45)]
        [TestCase((long)(-1), (byte)5, (ulong)561)]
        [TestCase((long)(-7), (byte)4, (ulong)0xFF)]
        [TestCase((long)(-7), (byte)5, (ulong)759)]
        public void PushBits_Int64_Test2(long bitsToPush, byte howMany, ulong desiredVal)
        {
            ulong result = 0;
            BitUtils.PushBitsSigned(bitsToPush, howMany, ref result);
            BitUtils.PushBitsSigned(bitsToPush, howMany, ref result);
            Assert.AreEqual(desiredVal, result);


            long got = BitUtils.PopBitsSigned(ref result, howMany);
            Assert.AreEqual(bitsToPush, got);

            got = BitUtils.PopBitsSigned(ref result, howMany);
            Assert.AreEqual(bitsToPush, got);
        }

        [TestCase((ulong)0xFFFFFFFF, 3, (byte)0x7, (byte)0x7, (byte)0x7, (byte)0x7)]
        [TestCase((ulong)0xFFFFFFFF, 5, (byte)0x1F, (byte)0x1F, (byte)0x1F)]
        public void PopByte_Test(ulong whatToPush, byte howManyBits, params byte[] args)
        {
            ulong reference = 0;
            BitUtils.PushBits(whatToPush, 32, ref reference);

            for (int i = 0; i < args.Length; i++)
            {
                var val = BitUtils.PopByte(ref reference, howManyBits);
                Assert.AreEqual(args[i], val);
            }
        }

        [TestCase((sbyte)(-1), 3, 4)]
        [TestCase((sbyte)(-1), 5, 6)]
        public void PopSByte_Test(sbyte whatToPush, byte howManyBits, int howManyTimes)
        {
            ulong reference = 0;
            for (int i = 0; i < howManyTimes; i++) { BitUtils.PushBitsSigned(whatToPush, howManyBits, ref reference); }

            for (int i = 0; i < howManyTimes; i++)
            {
                var val = BitUtils.PopSByte(ref reference, howManyBits);
                Assert.AreEqual(whatToPush, val);
            }
        }

        [TestCase((ushort)32, 7, 5)]
        [TestCase((ushort)63, 9, 3)]
        public void PopUInt16_Test(ushort whatToPush, byte howManyBits, int howManyTimes)
        {
            ulong reference = 0;
            for (int i = 0; i < howManyTimes; i++) { BitUtils.PushBits(whatToPush, howManyBits, ref reference); }

            for (int i = 0; i < howManyTimes; i++)
            {
                var val = BitUtils.PopUInt16(ref reference, howManyBits);
                Assert.AreEqual(whatToPush, val);
            }
        }

        [TestCase((short)(-32), 7, 5)]
        [TestCase((short)(-63), 9, 3)]
        public void PopInt16_Test(short whatToPush, byte howManyBits, int howManyTimes)
        {
            ulong reference = 0;
            for (int i = 0; i < howManyTimes; i++) { BitUtils.PushBitsSigned(whatToPush, howManyBits, ref reference); }

            for (int i = 0; i < howManyTimes; i++)
            {
                var val = BitUtils.PopInt16(ref reference, howManyBits);
                Assert.AreEqual(whatToPush, val);
            }
        }

        [TestCase((uint)(32), 7, 5)]
        [TestCase((uint)(63), 9, 3)]
        public void PopUInt32_Test(uint whatToPush, byte howManyBits, int howManyTimes)
        {
            ulong reference = 0;
            for (int i = 0; i < howManyTimes; i++) { BitUtils.PushBits(whatToPush, howManyBits, ref reference); }

            for (int i = 0; i < howManyTimes; i++)
            {
                var val = BitUtils.PopUInt32(ref reference, howManyBits);
                Assert.AreEqual(whatToPush, val);
            }
        }

        [TestCase(-32, 7, 5)]
        [TestCase(-63, 9, 3)]
        public void PopInt32_Test(int whatToPush, byte howManyBits, int howManyTimes)
        {
            ulong reference = 0;
            for (int i = 0; i < howManyTimes; i++) { BitUtils.PushBitsSigned(whatToPush, howManyBits, ref reference); }

            for (int i = 0; i < howManyTimes; i++)
            {
                var val = BitUtils.PopInt32(ref reference, howManyBits);
                Assert.AreEqual(whatToPush, val);
            }
        }

        [TestCase((ulong)(32), 7, 5)]
        [TestCase((ulong)(63), 9, 3)]
        public void PopUInt64_Test(ulong whatToPush, byte howManyBits, int howManyTimes)
        {
            ulong reference = 0;
            for (int i = 0; i < howManyTimes; i++) { BitUtils.PushBits(whatToPush, howManyBits, ref reference); }

            for (int i = 0; i < howManyTimes; i++)
            {
                var val = BitUtils.PopUInt64(ref reference, howManyBits);
                Assert.AreEqual(whatToPush, val);
            }
        }

        [TestCase((long)(-32), 7, 5)]
        [TestCase((long)(-63), 9, 3)]
        public void PopInt64_Test(long whatToPush, byte howManyBits, int howManyTimes)
        {
            ulong reference = 0;
            for (int i = 0; i < howManyTimes; i++) { BitUtils.PushBitsSigned(whatToPush, howManyBits, ref reference); }

            for (int i = 0; i < howManyTimes; i++)
            {
                var val = BitUtils.PopInt64(ref reference, howManyBits);
                Assert.AreEqual(whatToPush, val);
            }
        }
    }
}


#endif