﻿#if UNITY_EDITOR

using NUnit.Framework;
using GameSavvy.Byterizer;
using System;
using UnityEngine;

namespace UnitTests.Byterizer.Tests
{
	public class Arrays_AppendPop_Test
	{

		[TestCase(2, true, true, false, false, true, false)]
		[TestCase(3, true, true, false, false, true, false, true, false, true)]
		[TestCase(5, true, false, false, true, false, true, false, true, true, true, false, false, true, false, true, false, true, true, true, false, false, true, false, true, false, true)]
		public void BoolArr_Test(int len, params bool[] args)
		{
			var tmp = new ByteStream();
			tmp.AppendArray(args);
			var arr = tmp.PopBoolArray();
			Assert.AreEqual(tmp.Length, len);
			Assert.AreEqual(arr.Length, args.Length);
			Assert.AreEqual(arr, args);
			Assert.AreEqual(arr[1], args[1]);
		}

		[TestCase(6, (byte)1, (byte)2, (byte)1, (byte)2, (byte)3)]
		[TestCase(11, (byte)1, (byte)2, (byte)3, (byte)4, (byte)5, (byte)5, (byte)5, (byte)5, (byte)5, (byte)5)]
		[TestCase(3, (byte)0, (byte)1)]
		public void ByteArr_Test(int len, params byte[] args)
		{
			var tmp = new ByteStream();
			tmp.AppendArray(args);
			var arr = tmp.PopByteArray();
			Assert.AreEqual(tmp.Length, len);
			Assert.AreEqual(arr.Length, args.Length);
			Assert.AreEqual(arr, args);
			Assert.AreEqual(arr[1], args[1]);
		}

		[TestCase(29, "Hello", "World")]
		[TestCase(43, "Hello", "Z", "World", "08")]
		public void StringArr_Test(int len, params string[] args)
		{
			var tmp = new ByteStream();
			tmp.AppendArray(args);
			var arr = tmp.PopStringArray();
			Assert.AreEqual(tmp.Length, len);
			Assert.AreEqual(arr.Length, args.Length);
			Assert.AreEqual(arr, args);
			Assert.AreEqual(arr[1], args[1]);
		}

		[Test]
		public void MultiArray_Test()
		{
			//Init
			var tmp = new ByteStream();
			var tmp2 = new ByteStream();
			string[] strArr = new string[3] { "Hello", "World", "!" };
			float[] floatArr = new float[3] { 1f, 2f, 3f };
			Vector3[] vec3Arr = new Vector3[] { Vector3.up, Vector3.down, Vector3.right };

			//Append
			tmp.Append((byte)3);
			tmp.AppendArray(strArr, false);
			tmp.AppendArray(floatArr, false);
			tmp.AppendArray(vec3Arr, false);

			tmp2.Append((byte)3);
			tmp2.AppendArray(strArr);
			tmp2.AppendArray(floatArr);
			tmp2.AppendArray(vec3Arr);

			//Pop
			byte len = tmp.PopByte();
			var sArr = tmp.PopStringArray(len);
			var fArr = tmp.PopFloatArray(len);
			var v3Arr = tmp.PopVector3Array(len);

			//Assert Lengths
			Assert.AreEqual(len, 3);
			Assert.AreEqual(sArr.Length, strArr.Length);
			Assert.AreEqual(fArr.Length, floatArr.Length);
			Assert.AreEqual(v3Arr.Length, vec3Arr.Length);
			Assert.AreEqual(tmp.Length, tmp2.Length- 3);

			//Assert Content
			Assert.AreEqual(sArr[0], strArr[0]);
			Assert.AreEqual(fArr[0], floatArr[0]);
			Assert.AreEqual(v3Arr[0], vec3Arr[0]);
			Assert.AreEqual(sArr[1], strArr[1]);
			Assert.AreEqual(fArr[1], floatArr[1]);
			Assert.AreEqual(v3Arr[1], vec3Arr[1]);
			Assert.AreEqual(sArr[2], strArr[2]);
			Assert.AreEqual(fArr[2], floatArr[2]);
			Assert.AreEqual(v3Arr[2], vec3Arr[2]);
		}

		[TestCase(2, -1, (byte)1, (byte)2, (byte)3, (byte)4, (byte)5, (byte)6, (byte)7, (byte)8)]
		[TestCase(2, 3, (byte)1, (byte)2, (byte)3, (byte)4, (byte)5, (byte)6, (byte)7, (byte)8)]
		public void SubArray_Test(int index, int len, params byte[] args)
		{
			var subArr = ByteUtils.SubByteArray(args, index, len);
			for (int i = 0; i < subArr.Length; i++)
			{
				Assert.AreEqual(subArr[i], args[i + index]);
			}
		}

	}
}

#endif
